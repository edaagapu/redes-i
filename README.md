# Redes I

Taller I - Redes de Comunicaciones I

Edwin Aaron Garcia Pulido - 20161020051 <br>
Sebastian Mora Sabogal - 20161020031 <br>
Jhon Jairo Lancheros Rangel - 20162020077 <br>
Diego David Romero Quiroga - 20161020082 <br>

# Procedimiento de ejecución

1. Descargar el repositorio. <br>
2. Ejecutar según el sistema operativo. <br> 

# Linux

Se realiza mediante el comando " sudo python sniffer_lan.py " sobre la terminal, en la ubicación donde esta el archivo. <br>

# Windows

1. Descargar e instalar Python3. <br>
2. Abrir la terminal de Python con permisos de administrador. <br>
3. Ejecutar el archivo.
