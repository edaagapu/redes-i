'''
SNIFFER - Redes LAN

Edwin Aaron Garcia Pulido - 20161020051
Sebastian Mora Sabogal - 20161020031
Jhon Jairo Lancheros Rangel - 20162020077
Diego David Romero Quiroga - 20161020082
'''

import socket
import struct
import binascii

# Constantes para organizar la información

SEPA_AST = 50
SEPA_LINE = 35

def main():
    # Se crea la variable de conexión
    conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))

    # Conexión para el protocolo de resolución de direcciones (ARP)
    connARP = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(0x003))

    # Se hace un bucle sin fin para que reciba paquetes hasta que se interrumpa el proceso
    while (True):
        # Se reciben los datos del socket mediante recvfrom, con un tamaño cercano al máximo 65550
        data_raw, addr = conn.recvfrom(65550)
        dest_mac, src_mac, prot, data = capture_packages(data_raw)

        # IP
        if prot == 8:
            (version, header_size, ttl, proto, src, target, data) = ip_packet(data)
            #Se imprime el paquete IP con un formato definido
            print('*' * SEPA_AST)
            print('', 'Paquete IP:', sep='')
            band = 'Versión: {}, Tamaño del encabezado: {}, TTL: {}'.format(version, header_size, ttl)
            textf = band.split(',')
            for t in textf:
                print('', t, sep='\t')
            print(('\t'*1)+'Datos: ')
            formatear_texto('\t'*2, data)

            #ICMP
            if proto == 1:
                icmp_type, code, checksum, data = icmp_packet(data)
                print('-' * SEPA_LINE)
                print('','Paquete ICMP',sep='')
                band = 'Tipo: {}, Código: {}, Checksum: {}'.format(icmp_type, code, checksum)
                textf = band.split(',')
                for t in textf:
                    print('', t, sep='\t')
                print('\t'+ 'Datos: ')
                formatear_texto('\t' * 2, data)
                print('-' * SEPA_LINE)

            #TCP
            elif proto == 6:
                src_port, dest_port, sequence, acknowledgement, flag_urg, flag_ack, flag_psh, flag_rst, flag_syn, flag_fin, data = tcp_segment(data)
                print('-' * SEPA_LINE)
                print('', 'Segmento TCP', sep='')
                band = 'Puerto de origen: {}, Puerto de destino: {}, Secuencia: {}, Acknowlodegement: {}, Bandera: ,\t URG: {},\t ACK: {},\t PSH: {},\t RST: {},\t SYN: {},\t FIN: {}'\
                    .format(src_port, dest_port, sequence,acknowledgement, flag_urg, flag_ack, flag_psh, flag_rst, flag_syn, flag_fin)

                textf = band.split(',')
                for t in textf:
                    print('', t, sep='\t')
                print('\t' + 'Datos: ')
                formatear_texto('\t' * 2, data)
                print('-' * SEPA_LINE)

            #UDP
            elif proto == 17:
                src_port, dest_port, length, data = udp_segment(data)
                print('-' * SEPA_LINE)
                print('', 'Paquete ICMP', sep='')
                band = 'Puerto de origen: {}, Puerto de destino: {}, Longitud: {}'.format(src_port, dest_port, length)
                textf = band.split(',')
                for t in textf:
                    print('', t, sep='\t')
                print('\t'+ 'Datos: ')
                formatear_texto('\t' * 2, data)
                print('-' * SEPA_LINE)

            #Otro
            else:
                print('-' * SEPA_LINE)
                print('Datos: ')
                formatear_texto('\t', data)
                print('-' * SEPA_LINE)
            print('*' * SEPA_AST)
        elif prot != 1544:
            print('*' * SEPA_AST)
            print('Datos: ')
            formatear_texto('\t', data)
            print('*' * SEPA_AST)

        paqueteARP = connARP.recvfrom(2048)
        ethernet_header = paqueteARP[0][:14]
        ethernet_detalles = struct.unpack('!6s6s2s', ethernet_header)

        cabeceraARP = paqueteARP[0][14:42]
        arp_detalles = struct.unpack('2s2s1s1s2s6s4s6s4s', cabeceraARP)
        ethertype = ethernet_detalles[2]

        # Paquete ARP
        if ethertype == b'\x08\x06':
            print('\nPaquete ARP:')
            print('\t ' + 'Tipo de hardware: {}, Tipo de protocolo: {}'.format(
                str(binascii.hexlify(arp_detalles[0]), 'utf-8'),
                str(binascii.hexlify(arp_detalles[1]), 'utf-8')))
            print('\t ' + 'Tamaño del hardware: {}, Tamaño del protocolo: {}, opcode: {}'.format(
                str(binascii.hexlify(arp_detalles[2]), 'utf-8'),
                str(binascii.hexlify(arp_detalles[3]), 'utf-8'),
                str(binascii.hexlify(arp_detalles[4]), 'utf-8')))
            print('\t ' + 'Dirección MAC origen: {}, Dirección IP origen: {}'.format(
                str(binascii.hexlify(arp_detalles[5]), 'utf-8'),
                socket.inet_ntoa(arp_detalles[6])))
            print('\t ' + 'Dirección MAC destino: {}, Dirección IP destino: {}'.format(
                str(binascii.hexlify(arp_detalles[7]), 'utf-8'),
                socket.inet_ntoa(arp_detalles[8])))

def capture_packages(data):
    destination, source, protocol = struct.unpack('! 6s 6s H', data[:14])
    return get_mac_addr(destination), get_mac_addr(source), socket.htons(protocol), data[14:]


def get_mac_addr(bytes_addr):
    bytes_string = map('{:02x}'.format, bytes_addr)
    return ':'.join(bytes_string).upper()


def ip_packet(data):
    version_header_length = data[0]
    version = version_header_length >> 4  # Movimiento hacia la derecha
    header_length = (version_header_length & 15) * 4
    ttl, proto, src, target = struct.unpack('! 8x B B 2x 4s 4s', data[:20])

    return version, header_length, ttl, proto, ipv(src), ipv(target), data[header_length:]


def ipv(addr):
    return '.'.join(map(str, addr))


def icmp_packet(data):
    icmp_type, code, checksum = struct.unpack('! B B H', data[:4])

    return icmp_type, code, checksum, data[4:]


def tcp_segment(data):
    (source_port, dest_port, sequence, acknowledgement, offset_reserved_flags) = struct.unpack(
        '! H H L L H', data[:14])

    offset = (offset_reserved_flags >> 12) * 4
    bandera_urg = (offset_reserved_flags & 32) >> 5
    bandera_ack = (offset_reserved_flags & 16) >> 4
    bandera_psh = (offset_reserved_flags & 8) >> 3
    bandera_rst = (offset_reserved_flags & 4) >> 2
    bandera_syn = (offset_reserved_flags & 2) >> 1
    bandera_fin = offset_reserved_flags & 1

    return source_port, dest_port, sequence, acknowledgement, bandera_urg, bandera_ack, bandera_psh, bandera_rst, bandera_syn, bandera_fin, data[offset:]


def udp_segment(data):
    source_port, dest_port, size = struct.unpack('! H H 2x H', data[:8])
    return source_port, dest_port, size, data[8:]

def formatear_texto(sepa,texto):
    tamano  = len(texto)
    resultado = ''
    separ = 60
    fin = tamano//separ
    for i in range(0,fin-1):
        resultado = resultado + '\n' + sepa + str(texto[i*separ:((i+1)*separ) - 1])

    resultado = resultado + '\n' + sepa + str(texto[(fin-1)*separ:tamano])
    print(resultado)


if __name__ == '__main__':
    main()
