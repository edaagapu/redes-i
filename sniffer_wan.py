from socket import *
import struct
import sys
import re

#Proceso para recibir el datagrama

def receiveData(s):
    data = ''
    try:
        data = s.recvfrom(65565)
    except timeout:
        data = ''
    except:
        print("Ha ocurrido un error")
        sys.exc.info()
    return data[0]


#Proceso para obtener el tipo de servicio (8bits)

def getTOS(data):
    precedence = {0: 'Routine', 1:'Priority', 2:'Immediate', 3:'Flash', 4: 'Flash override', 5: 'CRITIC/ECP', 6: 'Internetwork control', 7:'Network control'}
    delay = {0: 'Normal delay', 1: 'Low delay'}
    throughput = {0: 'Normal throughtput', 1: 'High troughtput'}
    reliability = {0: 'Normal reliability', 1: 'High reliability'}
    cost = {0: 'Normal monetary cost', 1: 'Minimize monetary cost'}

    #Obtener los respectivos bits y hacer corrimiento a la derecha

    D = data&0x10
    D >>= 4

    T = data&0x8
    T >>= 3

    R = data&0x4
    R >>= 2

    M = data&0x2
    M >>= 1

    TOS = [precedence[data >> 5], delay[D], throughput[T], reliability[R], cost[M]]
    return TOS

